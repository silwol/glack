DROP VIEW IF EXISTS "view_months";
DROP TABLE IF EXISTS "workitems";
DROP TABLE IF EXISTS "workqueue";
DROP TABLE IF EXISTS "files";
DROP TABLE IF EXISTS "mimetypes";
DROP TABLE IF EXISTS "photos";
DROP TABLE IF EXISTS "photo_file_roles";
