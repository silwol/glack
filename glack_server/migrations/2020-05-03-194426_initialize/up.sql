CREATE TABLE "photo_file_roles"
(
    id VARCHAR NOT NULL PRIMARY KEY,
    is_artifact BOOL NOT NULL,
    is_thumbnail BOOL NOT NULL
);

INSERT INTO "photo_file_roles" (id, is_artifact, is_thumbnail) VALUES
('artifact', true, false),
('thumbnail', false, true);

CREATE TABLE "photos"
(
    uuid UUID PRIMARY KEY
);

CREATE TABLE "mimetypes"
(
    mimetype VARCHAR NOT NULL PRIMARY KEY,
    builtin BOOLEAN NOT NULL,
    can_build_thumbnail BOOL NOT NULL
);

INSERT INTO "mimetypes" (mimetype, builtin, can_build_thumbnail) VALUES
('image/jpeg', true, true),
('image/png', true, true);

CREATE TABLE "files"
(
    sha1sum BYTEA PRIMARY KEY CHECK(length(sha1sum) = 20),
    bytesize BIGINT NOT NULL CHECK(bytesize > 0),
    photo_uuid UUID NOT NULL REFERENCES photos(uuid),
    filename VARCHAR NOT NULL,
    mimetype VARCHAR NOT NULL REFERENCES mimetypes(mimetype),
    role VARCHAR NOT NULL REFERENCES photo_file_roles(id),
    moment TIMESTAMPTZ NULL
);

CREATE TABLE "workitems"
(
    photo_uuid UUID NOT NULL PRIMARY KEY REFERENCES photos(uuid) ON DELETE CASCADE,
    priority INT NOT NULL DEFAULT 0,
    claimed BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE VIEW "view_months" AS SELECT
CAST(EXTRACT(YEAR FROM moment) AS INT4) AS year,
CAST(EXTRACT(MONTH FROM moment) AS INT2) AS month,
COUNT(DISTINCT photo_uuid) AS count
FROM "files"
GROUP BY year, month;
