use crate::{GlackError, Result, Sha1Sum};
use actix_files::NamedFile;
use sha1::{Digest, Sha1};
use std::path::{Path, PathBuf};
use tempfile::NamedTempFile;

#[derive(Clone, Debug)]
pub struct FileStorage {
    path: PathBuf,
}

pub struct WriterWithSha1<W: std::io::Write> {
    inner: W,
    sha1: Sha1,
    count: usize,
}

impl WriterWithSha1<NamedTempFile> {
    pub fn path(&self) -> &Path {
        self.inner.path()
    }
}

impl<W: std::io::Write> std::io::Write for WriterWithSha1<W> {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let count = self.inner.write(buf)?;
        self.sha1.update(&buf[..count]);
        self.count += count;
        Ok(count)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        self.inner.flush()
    }
}

pub struct FileDetails {
    pub sha1sum: Sha1Sum,
    pub count: usize,
}

impl FileStorage {
    pub fn new(path: PathBuf) -> Self {
        Self { path }
    }

    fn files_path(&self) -> PathBuf {
        self.path.join("files")
    }

    fn temp_path(&self) -> PathBuf {
        self.path.join("tmp")
    }

    fn build_temp_dir(&self) -> Result<()> {
        Ok(std::fs::create_dir_all(&self.temp_path())?)
    }

    fn file_dir(&self, sha1sum: &Sha1Sum) -> PathBuf {
        self.files_path().join(&sha1sum.format_hex()[0..2])
    }

    pub fn file_path(&self, sha1sum: &Sha1Sum) -> PathBuf {
        let s = sha1sum.format_hex();
        self.files_path().join(&s[0..2]).join(&s[2..])
    }

    pub fn open_named_file(&self, sha1sum: &Sha1Sum) -> Result<NamedFile> {
        Ok(NamedFile::open(&self.file_path(sha1sum))?)
    }

    pub fn build_temp_file(&self) -> Result<WriterWithSha1<NamedTempFile>> {
        self.build_temp_dir()?;
        Ok(WriterWithSha1 {
            inner: NamedTempFile::new_in(&self.temp_path())?,
            sha1: Default::default(),
            count: 0usize,
        })
    }

    fn create_dir_for_file(&self, sha1sum: &Sha1Sum) -> Result<()> {
        Ok(std::fs::create_dir_all(&self.file_dir(sha1sum))?)
    }

    pub fn persist_temp_file(
        &self,
        tempfile: WriterWithSha1<NamedTempFile>,
    ) -> Result<FileDetails> {
        let WriterWithSha1 { inner, sha1, count } = tempfile;

        if count > i64::MAX as usize {
            return Err(GlackError::FileTooLarge);
        }

        let sha1sum = Sha1Sum::from_slice(&sha1.finalize())?;
        self.create_dir_for_file(&sha1sum)?;

        inner.persist(&self.file_path(&sha1sum))?;

        let details = FileDetails { sha1sum, count };
        Ok(details)
    }
}
