use crate::schema::*;
use chrono::{DateTime, Utc};
use uuid::Uuid;

#[derive(Serialize, Insertable, Queryable, Debug)]
pub struct File {
    pub sha1sum: Vec<u8>,
    pub bytesize: i64,
    pub photo_uuid: Uuid,
    pub filename: String,
    pub mimetype: String,
    pub role: String,
    pub moment: Option<DateTime<Utc>>,
}

#[derive(Serialize, Insertable, Queryable, Debug)]
pub struct Photo {
    pub uuid: Uuid,
}

#[derive(Serialize, Insertable, Queryable, Debug)]
#[table_name = "photo_file_roles"]
pub struct PhotoFileRole {
    pub id: String,
    pub is_artifact: bool,
    pub is_thumbnail: bool,
}

#[derive(Serialize, Insertable, Queryable, Debug)]
pub struct Mimetype {
    pub mimetype: String,
    pub builtin: bool,
    pub can_build_thumbnail: bool,
}

#[derive(Queryable, Debug)]
pub struct ViewMonth {
    pub year: Option<i32>,
    pub month: Option<i16>,
    pub count: i64,
}

#[derive(Serialize, Insertable, Debug)]
#[table_name = "workitems"]
pub struct InsertWorkitem {
    pub photo_uuid: Uuid,
}

#[derive(Serialize, Insertable, Queryable, Debug)]
pub struct Workitem {
    pub photo_uuid: Uuid,
    pub priority: i32,
    pub claimed: bool,
}
