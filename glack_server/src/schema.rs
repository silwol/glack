table! {
    files (sha1sum) {
        sha1sum -> Bytea,
        bytesize -> Int8,
        photo_uuid -> Uuid,
        filename -> Varchar,
        mimetype -> Varchar,
        role -> Varchar,
        moment -> Nullable<Timestamptz>,
    }
}

table! {
    mimetypes (mimetype) {
        mimetype -> Varchar,
        builtin -> Bool,
        can_build_thumbnail -> Bool,
    }
}

table! {
    photo_file_roles (id) {
        id -> Varchar,
        is_artifact -> Bool,
        is_thumbnail -> Bool,
    }
}

table! {
    photos (uuid) {
        uuid -> Uuid,
    }
}

table! {
    workitems (photo_uuid) {
        photo_uuid -> Uuid,
        priority -> Int4,
        claimed -> Bool,
    }
}

joinable!(files -> mimetypes (mimetype));
joinable!(files -> photo_file_roles (role));
joinable!(files -> photos (photo_uuid));
joinable!(workitems -> photos (photo_uuid));

allow_tables_to_appear_in_same_query!(
    files,
    mimetypes,
    photo_file_roles,
    photos,
    workitems,
);
