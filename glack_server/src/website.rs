use crate::actions::Actions;
use crate::filestorage::{self, FileStorage};
use crate::{
    models, DbPool, GlackError, Paging, ProgramInfo, Result, Sha1Sum,
    WorkItemSender, PAGE_SIZE,
};
use actix_files::NamedFile;
use actix_http::http::header::{
    ContentDisposition, DispositionParam, DispositionType,
};
use actix_http::ResponseBuilder;
use actix_multipart::Multipart;
use actix_web::http::StatusCode;
use actix_web::{get, http, put, web, HttpResponse, Responder};
use askama::Template;
use chrono::{DateTime, Local, NaiveDateTime, Utc};
use crossbeam_channel::{bounded, Sender};
use futures::StreamExt;
use log::warn;
use std::collections::HashMap;
use std::io::Write;
use std::path::Path;
use uuid::Uuid;

pub fn config(
    conf: &mut web::ServiceConfig,
    db_pool: DbPool,
    storage: FileStorage,
    work_item_sender: WorkItemSender,
    notification_register_sender: Sender<(Uuid, Sender<Sha1Sum>)>,
) -> &mut web::ServiceConfig {
    conf.data(db_pool)
        .data(storage)
        .data(work_item_sender)
        .data(notification_register_sender)
        .service(get_root)
        .service(get_photo_uuid)
        .service(get_photo_uuid_file_sha1sum)
        .service(get_photo_uuid_thumbnail)
        .service(put_photo)
        .service(get_date_none)
        .service(get_date_none_thumbnail)
        .service(get_date_year)
        .service(get_date_year_month)
        .service(get_date_year_month_thumbnail)
        .service(get_css)
}

#[derive(Debug, PartialEq, Template)]
#[template(path = "err_404.html")]
struct ErrTemplate {
    programinfo: ProgramInfo,
    statuscode: StatusCode,
    message: String,
}

async fn err_404_template() -> Result<ErrTemplate> {
    Ok(ErrTemplate {
        programinfo: ProgramInfo::default(),
        statuscode: StatusCode::NOT_FOUND,
        message: "Page not found".to_string(),
    })
}

pub async fn err_404() -> Result<impl Responder> {
    let template = err_404_template().await?;
    let render = web::block(move || template.render()).await?;
    Ok(HttpResponse::NotFound()
        .content_type("text/html")
        .body(render))
}

#[derive(Debug, PartialEq, Template)]
#[template(path = "index.html")]
struct IndexTemplate {
    programinfo: ProgramInfo,
    months: Vec<ViewMonthInfo>,
}

async fn get_root_template<A: Actions>(a: &A) -> Result<IndexTemplate> {
    let months = a
        .get_view_months()?
        .into_iter()
        .map(ViewMonthInfo::from)
        .collect();
    Ok(IndexTemplate {
        programinfo: ProgramInfo::default(),
        months,
    })
}

#[get("/")]
async fn get_root(pool: web::Data<DbPool>) -> Result<impl Responder> {
    let template = get_root_template(pool.as_ref()).await?;
    let render = web::block(move || template.render()).await?;
    Ok(HttpResponse::Ok().content_type("text/html").body(render))
}

#[derive(Template)]
#[template(path = "photo.html")]
struct PhotoTemplate {
    programinfo: ProgramInfo,
    uuid: Uuid,
    items: Vec<PhotoItem>,
}

struct PhotoItem {
    sha1sum: String,
    filename: String,
    size: String,
    role: String,
    moment: String,
}

async fn get_photo_uuid_impl<A: Actions>(
    a: &A,
    uuid: Uuid,
) -> Result<PhotoTemplate> {
    let items = a
        .get_files_for_photo(uuid)?
        .into_iter()
        .map(|file| {
            use humansize::{file_size_opts, FileSize};
            let sha1sum = Sha1Sum::from_slice(&file.sha1sum)?.format_hex();
            Ok(PhotoItem {
                sha1sum,
                filename: file.filename,
                role: file.role,
                size: file
                    .bytesize
                    .file_size(file_size_opts::CONVENTIONAL)
                    .unwrap(),
                moment: match file.moment {
                    Some(moment) => moment.format("%+").to_string(),
                    None => "Unknown moment".to_string(),
                },
            })
        })
        .collect::<Result<Vec<_>>>()?;
    Ok(PhotoTemplate {
        programinfo: ProgramInfo::default(),
        uuid,
        items,
    })
}

#[get("/photo/{uuid}")]
async fn get_photo_uuid(
    pool: web::Data<DbPool>,
    uuid: web::Path<Uuid>,
) -> Result<impl Responder> {
    let template =
        get_photo_uuid_impl(pool.as_ref(), uuid.into_inner()).await?;
    let render = web::block(move || template.render()).await?;

    Ok(HttpResponse::Ok().content_type("text/html").body(render))
}

async fn get_photo_uuid_file_sha1sum_named_file<A: Actions>(
    a: &A,
    storage: &FileStorage,
    uuid: Uuid,
    sha1sum: &Sha1Sum,
) -> Result<NamedFile> {
    let file = a.get_file(sha1sum)?;

    if file.photo_uuid != uuid {
        return Err(GlackError::NotFound);
    }

    let named_file = storage.open_named_file(&sha1sum)?;

    let disposition = ContentDisposition {
        disposition: DispositionType::Inline,
        parameters: vec![DispositionParam::Filename(
            file.filename.to_string(),
        )],
    };
    let mimetype = file
        .mimetype
        .parse::<mime::Mime>()
        .unwrap_or(mime::APPLICATION_OCTET_STREAM);

    Ok(named_file
        .set_content_disposition(disposition)
        .set_content_type(mimetype))
}

#[get("/photo/{uuid}/file/{sha1sum}")]
async fn get_photo_uuid_file_sha1sum(
    pool: web::Data<DbPool>,
    storage: web::Data<FileStorage>,
    params: web::Path<UuidAndSha1sum>,
) -> Result<impl Responder> {
    let UuidAndSha1sum { uuid, sha1sum } = params.into_inner();

    let named_file = get_photo_uuid_file_sha1sum_named_file(
        pool.as_ref(),
        storage.as_ref(),
        uuid,
        &sha1sum,
    )
    .await?;

    Ok(named_file)
}

async fn get_photo_uuid_thumbnail_sha1sum<A: Actions>(
    a: &A,
    notification_register_sender: &Sender<(Uuid, Sender<Sha1Sum>)>,
    work_item_sender: &WorkItemSender,
    uuid: Uuid,
) -> Result<Sha1Sum> {
    match a.get_photo_thumbnail(uuid).ok() {
        Some((file, _role)) => Ok(Sha1Sum::from_slice(&file.sha1sum)?),
        None => {
            let (notification_sender, notification_receiver) = bounded(1);

            let workitem = models::InsertWorkitem { photo_uuid: uuid };
            a.insert_or_prioritize_workitem(workitem)?;
            match notification_register_sender
                .send((uuid, notification_sender))
            {
                Ok(()) => {
                    // Wake up a worker. If all workers are busy, they will process
                    // the work queue after they finished their last item, so no
                    // need to check whether the sending went ok.
                    work_item_sender.try_send(()).ok();
                    let notification =
                        web::block(move || notification_receiver.recv())
                            .await;
                    match notification {
                        Ok(sha1sum) => Ok(sha1sum),
                        Err(e) => {
                            warn!(
                                "Couldn't receive notification for photo {} \
                        thumbnail generation, probably the extraction \
                        process is not running?\n{:?}",
                                uuid, e
                            );
                            Err(GlackError::NotFound)
                        }
                    }
                }
                Err(e) => {
                    warn!(
                        "Couldn't send notification request for photo {} \
                        thumbnail generation, probably the extraction \
                        process is not running?\n{:?}",
                        uuid, e
                    );
                    Err(GlackError::NotFound)
                }
            }
        }
    }
}

#[get("/photo/{uuid}/thumbnail")]
async fn get_photo_uuid_thumbnail(
    pool: web::Data<DbPool>,
    notification_register_sender: web::Data<Sender<(Uuid, Sender<Sha1Sum>)>>,
    work_item_sender: web::Data<WorkItemSender>,
    uuid: web::Path<Uuid>,
) -> Result<impl Responder> {
    let uuid = uuid.into_inner();

    let sha1sum = get_photo_uuid_thumbnail_sha1sum(
        pool.as_ref(),
        notification_register_sender.as_ref(),
        work_item_sender.as_ref(),
        uuid,
    )
    .await?;

    Ok(actix_http::Response::TemporaryRedirect()
        .header(
            actix_web::http::header::LOCATION,
            format!("/photo/{}/file/{}", uuid, sha1sum.format_hex()),
        )
        .body("Temporary redirect"))
}

async fn put_photo_impl<A: Actions>(
    a: &A,
    storage: &FileStorage,
    work_item_sender: &WorkItemSender,
    mut payload: Multipart,
) -> Result<Option<Uuid>> {
    use futures_util::stream::TryStreamExt;
    let mut db_files = Vec::new();
    let mut photo_uuid = None;
    let mut existing_photo = false;

    let mut db_mimetypes = a
        .get_mimetypes()?
        .into_iter()
        .map(|m| (m.mimetype.to_string(), m))
        .collect::<HashMap<String, models::Mimetype>>();

    while let Ok(Some(mut field)) = payload.try_next().await {
        if let Some(content_type) = field.content_disposition() {
            if let Some(filename) = content_type.get_filename() {
                let mut tempfile = storage.build_temp_file()?;

                while let Some(chunk) = field.next().await {
                    let data = chunk.unwrap();
                    tempfile.write_all(&data)?;
                }

                let mimetype = xdg_utils::query_mime_info(tempfile.path())
                    .map(|bytes| String::from_utf8_lossy(&bytes).into_owned())
                    .unwrap_or_else(|_| {
                        "application/octet-stream".to_string()
                    });
                let metadata = load_metadata(tempfile.path())?;

                let filestorage::FileDetails { sha1sum, count } =
                    storage.persist_temp_file(tempfile)?;

                db_mimetypes.entry(mimetype.to_string()).or_insert_with(
                    || models::Mimetype {
                        mimetype: mimetype.to_string(),
                        builtin: false,
                        can_build_thumbnail: false,
                    },
                );

                let existing_file = a.get_file(&sha1sum).ok();

                photo_uuid = match (existing_file, photo_uuid) {
                    (Some(existing_file), Some(photo_uuid))
                        if existing_file.photo_uuid != photo_uuid =>
                    {
                        return Err(GlackError::InconsistentPhotoAssignment);
                    }
                    (Some(existing_file), _) => {
                        existing_photo = true;
                        Some(existing_file.photo_uuid)
                    }
                    (None, Some(photo_uuid)) => {
                        let file = models::File {
                            sha1sum: sha1sum.to_vec(),
                            bytesize: count as i64,
                            photo_uuid,
                            filename: filename.to_string(),
                            mimetype,
                            role: "artifact".to_string(),
                            moment: metadata.moment,
                        };
                        db_files.push(file);
                        Some(photo_uuid)
                    }
                    (None, None) => {
                        let photo_uuid = Uuid::new_v4();
                        let file = models::File {
                            sha1sum: sha1sum.to_vec(),
                            bytesize: count as i64,
                            photo_uuid,
                            filename: filename.to_string(),
                            mimetype,
                            role: "artifact".to_string(),
                            moment: metadata.moment,
                        };
                        db_files.push(file);
                        Some(photo_uuid)
                    }
                };
            }
        }
    }

    if let Some(photo_uuid) = photo_uuid {
        let c = a;
        a.transaction(|| {
            if !existing_photo {
                c.insert_photos(&[models::Photo { uuid: photo_uuid }])?;
            }
            c.insert_mimetypes(
                &db_mimetypes.drain().map(|(_k, v)| v).collect::<Vec<_>>(),
            )?;
            c.insert_files(&db_files)?;
            c.insert_workitem(models::InsertWorkitem { photo_uuid })?;
            // Wake up a worker. If all workers are busy, they will process
            // the work queue after they finished their last item, so no
            // need to check whether the sending went ok.
            work_item_sender.try_send(()).ok();
            Ok(())
        })?;

        Ok(Some(photo_uuid))
    } else {
        Ok(None)
    }
}

#[put("/photo")]
async fn put_photo(
    pool: web::Data<DbPool>,
    storage: web::Data<FileStorage>,
    work_item_sender: web::Data<WorkItemSender>,
    payload: Multipart,
) -> Result<web::HttpResponse, GlackError> {
    let msg = match put_photo_impl(
        pool.as_ref(),
        storage.as_ref(),
        work_item_sender.as_ref(),
        payload,
    )
    .await?
    {
        Some(uuid) => format!("{}\n", uuid),
        None => "Nothing uploaded".to_string(),
    };

    Ok(actix_http::Response::Ok().body(msg))
}

#[derive(Template)]
#[template(path = "date_none.html")]
struct WithoutMomentTemplate {
    programinfo: ProgramInfo,
    crumbtrail: Vec<Crumb>,
    photos: Vec<models::Photo>,
    paging: Paging,
}

async fn get_date_none_impl<A: Actions>(
    a: &A,
    page: usize,
) -> Result<WithoutMomentTemplate> {
    let view_info = a.get_view_month_none()?;
    let paging = Paging {
        page,
        page_size: PAGE_SIZE,
        count: view_info.count as usize,
    };
    let photos = a.get_photos_without_moment(paging)?;

    Ok(WithoutMomentTemplate {
        programinfo: ProgramInfo::default(),
        crumbtrail: vec![
            Crumb {
                slug: "/".to_string(),
                label: "All photos".to_string(),
            },
            Crumb {
                slug: "/date/none".to_string(),
                label: "Without date".to_string(),
            },
        ],
        photos,
        paging,
    })
}

#[get("/date/none")]
async fn get_date_none(
    pool: web::Data<DbPool>,
    web::Query(paging_params): web::Query<PagingParams>,
) -> Result<impl Responder> {
    let page = paging_params.page.unwrap_or_default();
    let template = get_date_none_impl(pool.as_ref(), page).await?;
    let render = web::block(move || template.render()).await?;
    Ok(HttpResponse::Ok().content_type("text/html").body(render))
}

#[derive(Template)]
#[template(path = "date_year_month.html")]
struct YearMonthTemplate {
    programinfo: ProgramInfo,
    crumbtrail: Vec<Crumb>,
    year: u16,
    month_name: String,
    photos: Vec<models::Photo>,
    paging: Paging,
}

async fn get_date_year_month_template<A: Actions>(
    a: &A,
    page: usize,
    year: u16,
    month: u8,
) -> Result<YearMonthTemplate> {
    let month_info = a.get_view_month(year as i32, month as i16)?;
    let paging = Paging {
        page,
        page_size: PAGE_SIZE,
        count: month_info.count as usize,
    };

    use chrono::TimeZone;
    let start = Utc.ymd(year as i32, month as u32, 1).and_hms(0, 0, 0);
    let end = start.add_month();

    let month_name = start.format("%B").to_string();

    let photos = a.get_photos_for_timeframe(start, end, paging)?;

    Ok(YearMonthTemplate {
        programinfo: ProgramInfo::default(),
        crumbtrail: vec![
            Crumb {
                slug: "/".to_string(),
                label: "All photos".to_string(),
            },
            Crumb {
                slug: format!("/date/{:04}", year),
                label: format!("{:04}", year),
            },
            Crumb {
                slug: format!("/date/{:04}/{:02}", year, month),
                label: month_name.to_string(),
            },
        ],
        year,
        month_name,
        photos,
        paging,
    })
}

#[get("/date/{year}/{month}")]
async fn get_date_year_month(
    pool: web::Data<DbPool>,
    params: web::Path<(u16, u8)>,
    web::Query(paging_params): web::Query<PagingParams>,
) -> Result<impl Responder> {
    let page = paging_params.page.unwrap_or_default();
    let (year, month) = params.into_inner();

    let template =
        get_date_year_month_template(pool.as_ref(), page, year, month)
            .await?;
    let render = web::block(move || template.render()).await?;
    Ok(HttpResponse::Ok().content_type("text/html").body(render))
}

#[derive(Template)]
#[template(path = "date_year.html")]
struct YearTemplate {
    programinfo: ProgramInfo,
    crumbtrail: Vec<Crumb>,
    year: u16,
    months: Vec<u16>,
}

async fn get_date_year_impl<A: Actions>(
    a: &A,
    year: u16,
) -> Result<YearTemplate> {
    let months = a.get_view_year_months(year as i32)?;

    Ok(YearTemplate {
        programinfo: ProgramInfo::default(),
        crumbtrail: vec![
            Crumb {
                slug: "/".to_string(),
                label: "All photos".to_string(),
            },
            Crumb {
                slug: format!("/date/{:04}", year),
                label: format!("{:04}", year),
            },
        ],
        year,
        months: months
            .into_iter()
            .map(|m| m.month.unwrap() as u16)
            .collect(),
    })
}

#[get("/date/{year}")]
async fn get_date_year(
    pool: web::Data<DbPool>,
    params: web::Path<u16>,
) -> Result<impl Responder> {
    let template =
        get_date_year_impl(pool.as_ref(), params.into_inner()).await?;
    let render = web::block(move || template.render()).await?;
    Ok(HttpResponse::Ok().content_type("text/html").body(render))
}

async fn get_date_none_thumbnail_impl<A: Actions>(
    a: &A,
) -> Result<Option<Uuid>> {
    let paging = Paging {
        page: 0,
        page_size: 1,
        count: 1,
    };

    let photos = a.get_photos_without_moment(paging)?;

    Ok(photos.get(0).map(|p| p.uuid))
}

#[get("/date/none/thumbnail")]
async fn get_date_none_thumbnail(
    pool: web::Data<DbPool>,
) -> Result<impl Responder> {
    Ok(match get_date_none_thumbnail_impl(pool.as_ref()).await? {
        Some(uuid) => actix_http::Response::TemporaryRedirect()
            .header(
                actix_web::http::header::LOCATION,
                format!("/photo/{}/thumbnail", uuid,),
            )
            .body("Temporary redirect"),
        None => actix_http::Response::NotFound().body("Not found"),
    })
}

async fn get_date_year_month_thumbnail_impl<A: Actions>(
    a: &A,
    year: u16,
    month: u8,
) -> Result<Option<Uuid>> {
    let paging = Paging {
        page: 0,
        page_size: 1,
        count: 1,
    };

    use chrono::TimeZone;
    let start = Utc.ymd(year as i32, month as u32, 1).and_hms(0, 0, 0);
    let end = start.add_month();

    let photos = a.get_photos_for_timeframe(start, end, paging)?;

    Ok(photos.get(0).map(|p| p.uuid))
}

#[get("/date/{year}/{month}/thumbnail")]
async fn get_date_year_month_thumbnail(
    pool: web::Data<DbPool>,
    params: web::Path<(u16, u8)>,
) -> Result<impl Responder> {
    let (year, month) = params.into_inner();

    Ok(
        match get_date_year_month_thumbnail_impl(pool.as_ref(), year, month)
            .await?
        {
            Some(uuid) => actix_http::Response::TemporaryRedirect()
                .header(
                    actix_web::http::header::LOCATION,
                    format!("/photo/{}/thumbnail", uuid,),
                )
                .body("Temporary redirect"),
            None => actix_http::Response::NotFound().body("Not found"),
        },
    )
}

#[derive(Template)]
#[template(path = "style.css", escape = "none")]
struct StyleTemplate {
    programinfo: ProgramInfo,
}

async fn get_css_impl() -> StyleTemplate {
    StyleTemplate {
        programinfo: ProgramInfo::default(),
    }
}

#[get("/css")]
async fn get_css() -> Result<impl Responder> {
    let template = get_css_impl().await;
    let render = web::block(move || template.render()).await?;
    Ok(HttpResponse::Ok().content_type("text/css").body(render))
}

#[derive(Debug, PartialEq)]
struct YearMonth {
    pub y: i32,
    pub m: i16,
}

#[derive(Debug, PartialEq)]
struct ViewMonthInfo {
    pub year_month: Option<YearMonth>,
    pub count: i64,
}

impl From<models::ViewMonth> for ViewMonthInfo {
    fn from(m: models::ViewMonth) -> Self {
        let models::ViewMonth { year, month, count } = m;
        match (year, month) {
            (Some(y), Some(m)) => ViewMonthInfo {
                year_month: Some(YearMonth { y, m }),
                count,
            },
            _ => ViewMonthInfo {
                year_month: None,
                count,
            },
        }
    }
}

#[derive(Debug, Deserialize)]
struct UuidAndSha1sum {
    uuid: Uuid,
    sha1sum: Sha1Sum,
}

fn load_metadata(path: &Path) -> Result<PhotoMetadata> {
    let metadata = rexiv2::Metadata::new_from_path(path);
    match metadata {
        Ok(metadata) if metadata.supports_exif() => {
            use chrono::offset::TimeZone;

            if let Ok(datetimestr) =
                metadata.get_tag_string("Exif.Image.DateTime")
            {
                match NaiveDateTime::parse_from_str(
                    &datetimestr,
                    "%Y:%m:%d %H:%M:%S",
                ) {
                    Ok(naive) => {
                        let local: DateTime<Local> =
                            Local.from_local_datetime(&naive).unwrap();
                        let moment: DateTime<Utc> = local.into();
                        Ok(PhotoMetadata {
                            moment: Some(moment),
                        })
                    }
                    Err(e) => {
                        warn!("Error parsing date string: {:?}", e);
                        Ok(PhotoMetadata::default())
                    }
                }
            } else {
                Ok(PhotoMetadata::default())
            }
        }
        Ok(_) => Ok(PhotoMetadata::default()),
        Err(e) => {
            warn!("Error reading metadata: {:?}", e);
            Ok(PhotoMetadata::default())
        }
    }
}

#[derive(Default)]
struct PhotoMetadata {
    moment: Option<DateTime<Utc>>,
}

#[derive(Deserialize)]
pub struct PagingParams {
    page: Option<usize>,
}

trait AddMonth {
    fn add_month(&self) -> Self;
}

impl<T: chrono::Datelike> AddMonth for T {
    fn add_month(&self) -> Self {
        let month = self.month0() + 1;
        if month >= 12 {
            let year = self.year() + 1;
            self.with_month0(0).unwrap().with_year(year).unwrap()
        } else {
            self.with_month0(month).unwrap()
        }
    }
}

pub struct Crumb {
    slug: String,
    label: String,
}

impl actix_web::error::ResponseError for GlackError {
    fn error_response(&self) -> HttpResponse {
        let template = ErrTemplate {
            programinfo: ProgramInfo::default(),
            statuscode: self.status_code(),
            message: self.to_string(),
        };
        let render = template.render().unwrap();

        ResponseBuilder::new(self.status_code())
            .set_header(
                http::header::CONTENT_TYPE,
                "text/html; charset=utf-8",
            )
            .body(render)
    }
    fn status_code(&self) -> http::StatusCode {
        match *self {
            GlackError::NotFound => http::StatusCode::NOT_FOUND,
            _ => http::StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

trait GetConnection {
    type Connection;
    fn get_connection(&self) -> Result<Self::Connection>;
}

impl GetConnection for DbPool {
    type Connection = r2d2::PooledConnection<
        diesel::r2d2::ConnectionManager<diesel::PgConnection>,
    >;
    fn get_connection(&self) -> Result<Self::Connection> {
        Ok(self.get()?)
    }
}

impl Actions for DbPool {
    fn transaction<T, F: FnOnce() -> Result<T>>(&self, f: F) -> Result<T> {
        self.get_connection()?.transaction(f)
    }

    fn get_view_months(&self) -> Result<Vec<models::ViewMonth>> {
        self.get_connection()?.get_view_months()
    }
    fn get_files_for_photo(
        &self,
        photo_uuid: Uuid,
    ) -> Result<Vec<models::File>> {
        self.get_connection()?.get_files_for_photo(photo_uuid)
    }
    fn get_file(&self, sha1sum: &Sha1Sum) -> Result<models::File> {
        self.get_connection()?.get_file(sha1sum)
    }
    fn get_photo_thumbnail(
        &self,
        photo_uuid: Uuid,
    ) -> Result<(models::File, models::PhotoFileRole)> {
        self.get_connection()?.get_photo_thumbnail(photo_uuid)
    }
    fn get_file_for_photo_thumbnail_creation(
        &self,
        photo_uuid: Uuid,
    ) -> Result<(models::File, models::Mimetype)> {
        self.get_connection()?
            .get_file_for_photo_thumbnail_creation(photo_uuid)
    }
    fn insert_files(&self, files: &[models::File]) -> Result<()> {
        self.get_connection()?.insert_files(files)
    }
    fn get_mimetypes(&self) -> Result<Vec<models::Mimetype>> {
        self.get_connection()?.get_mimetypes()
    }
    fn insert_photos(&self, photos: &[models::Photo]) -> Result<()> {
        self.get_connection()?.insert_photos(photos)
    }
    fn insert_mimetypes(&self, mimetypes: &[models::Mimetype]) -> Result<()> {
        self.get_connection()?.insert_mimetypes(mimetypes)
    }
    fn insert_workitem(&self, item: models::InsertWorkitem) -> Result<()> {
        self.get_connection()?.insert_workitem(item)
    }
    fn insert_or_prioritize_workitem(
        &self,
        item: models::InsertWorkitem,
    ) -> Result<()> {
        self.get_connection()?.insert_or_prioritize_workitem(item)
    }
    fn get_first_unclaimed_workitem(
        &self,
    ) -> Result<Option<models::Workitem>> {
        self.get_connection()?.get_first_unclaimed_workitem()
    }
    fn claim_workitem(&self, uuid: Uuid) -> Result<()> {
        self.get_connection()?.claim_workitem(uuid)
    }
    fn remove_workitem(&self, uuid: Uuid) -> Result<()> {
        self.get_connection()?.remove_workitem(uuid)
    }
    fn get_view_month_none(&self) -> Result<models::ViewMonth> {
        self.get_connection()?.get_view_month_none()
    }
    fn get_photos_without_moment(
        &self,
        paging: Paging,
    ) -> Result<Vec<models::Photo>> {
        self.get_connection()?.get_photos_without_moment(paging)
    }
    fn get_view_month(
        &self,
        year: i32,
        month: i16,
    ) -> Result<models::ViewMonth> {
        self.get_connection()?.get_view_month(year, month)
    }
    fn get_photos_for_timeframe(
        &self,
        start: DateTime<Utc>,
        end: DateTime<Utc>,
        paging: Paging,
    ) -> Result<Vec<models::Photo>> {
        self.get_connection()?
            .get_photos_for_timeframe(start, end, paging)
    }
    fn get_view_year_months(
        &self,
        year: i32,
    ) -> Result<Vec<models::ViewMonth>> {
        self.get_connection()?.get_view_year_months(year)
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        actions::Actions, models, Paging, ProgramInfo, Result, Sha1Sum,
    };
    use chrono::{DateTime, Utc};
    use uuid::Uuid;

    #[tokio::test]
    async fn test_get_root_template() {
        use super::{
            get_root_template, IndexTemplate, ViewMonthInfo, YearMonth,
        };

        struct Mock;

        impl ActionsMock for Mock {
            fn get_view_months(&self) -> Result<Vec<models::ViewMonth>> {
                Ok(vec![models::ViewMonth {
                    year: Some(2000),
                    month: Some(9),
                    count: 30,
                }])
            }
        }

        let mock = Mock;

        assert_eq!(
            get_root_template(&mock).await.unwrap(),
            IndexTemplate {
                programinfo: ProgramInfo::default(),
                months: vec![ViewMonthInfo {
                    year_month: Some(YearMonth { y: 2000, m: 9 }),
                    count: 30,
                }]
            }
        );
    }

    pub trait ActionsMock {
        fn transaction<T, F: FnOnce() -> Result<T>>(
            &self,
            _f: F,
        ) -> Result<T> {
            unimplemented!()
        }
        fn get_view_months(&self) -> Result<Vec<models::ViewMonth>> {
            unimplemented!()
        }
        fn get_files_for_photo(
            &self,
            _photo_uuid: Uuid,
        ) -> Result<Vec<models::File>> {
            unimplemented!()
        }
        fn get_file(&self, _sha1sum: &Sha1Sum) -> Result<models::File> {
            unimplemented!()
        }
        fn get_photo_thumbnail(
            &self,
            _photo_uuid: Uuid,
        ) -> Result<(models::File, models::PhotoFileRole)> {
            unimplemented!()
        }
        fn get_file_for_photo_thumbnail_creation(
            &self,
            _photo_uuid: Uuid,
        ) -> Result<(models::File, models::Mimetype)> {
            unimplemented!()
        }
        fn insert_files(&self, _files: &[models::File]) -> Result<()> {
            unimplemented!()
        }
        fn get_mimetypes(&self) -> Result<Vec<models::Mimetype>> {
            unimplemented!()
        }
        fn insert_photos(&self, _photos: &[models::Photo]) -> Result<()> {
            unimplemented!()
        }
        fn insert_mimetypes(
            &self,
            _mimetypes: &[models::Mimetype],
        ) -> Result<()> {
            unimplemented!()
        }
        fn insert_workitem(
            &self,
            _item: models::InsertWorkitem,
        ) -> Result<()> {
            unimplemented!()
        }
        fn insert_or_prioritize_workitem(
            &self,
            _item: models::InsertWorkitem,
        ) -> Result<()> {
            unimplemented!()
        }
        fn get_first_unclaimed_workitem(
            &self,
        ) -> Result<Option<models::Workitem>> {
            unimplemented!()
        }
        fn claim_workitem(&self, _uuid: Uuid) -> Result<()> {
            unimplemented!()
        }
        fn remove_workitem(&self, _uuid: Uuid) -> Result<()> {
            unimplemented!()
        }
        fn get_view_month_none(&self) -> Result<models::ViewMonth> {
            unimplemented!()
        }
        fn get_photos_without_moment(
            &self,
            _paging: Paging,
        ) -> Result<Vec<models::Photo>> {
            unimplemented!()
        }
        fn get_view_month(
            &self,
            _year: i32,
            _month: i16,
        ) -> Result<models::ViewMonth> {
            unimplemented!()
        }
        fn get_photos_for_timeframe(
            &self,
            _start: DateTime<Utc>,
            _end: DateTime<Utc>,
            _paging: Paging,
        ) -> Result<Vec<models::Photo>> {
            unimplemented!()
        }
        fn get_view_year_months(
            &self,
            _year: i32,
        ) -> Result<Vec<models::ViewMonth>> {
            unimplemented!()
        }
    }
    impl<M: ActionsMock> Actions for M {
        fn transaction<T, F: FnOnce() -> Result<T>>(
            &self,
            f: F,
        ) -> Result<T> {
            M::transaction(self, f)
        }

        fn get_view_months(&self) -> Result<Vec<models::ViewMonth>> {
            M::get_view_months(self)
        }
        fn get_files_for_photo(
            &self,
            photo_uuid: Uuid,
        ) -> Result<Vec<models::File>> {
            M::get_files_for_photo(self, photo_uuid)
        }
        fn get_file(&self, sha1sum: &Sha1Sum) -> Result<models::File> {
            M::get_file(self, sha1sum)
        }
        fn get_photo_thumbnail(
            &self,
            photo_uuid: Uuid,
        ) -> Result<(models::File, models::PhotoFileRole)> {
            M::get_photo_thumbnail(self, photo_uuid)
        }
        fn get_file_for_photo_thumbnail_creation(
            &self,
            photo_uuid: Uuid,
        ) -> Result<(models::File, models::Mimetype)> {
            M::get_file_for_photo_thumbnail_creation(self, photo_uuid)
        }
        fn insert_files(&self, files: &[models::File]) -> Result<()> {
            M::insert_files(self, files)
        }
        fn get_mimetypes(&self) -> Result<Vec<models::Mimetype>> {
            M::get_mimetypes(self)
        }
        fn insert_photos(&self, photos: &[models::Photo]) -> Result<()> {
            M::insert_photos(self, photos)
        }
        fn insert_mimetypes(
            &self,
            mimetypes: &[models::Mimetype],
        ) -> Result<()> {
            M::insert_mimetypes(self, mimetypes)
        }
        fn insert_workitem(
            &self,
            item: models::InsertWorkitem,
        ) -> Result<()> {
            M::insert_workitem(self, item)
        }
        fn insert_or_prioritize_workitem(
            &self,
            item: models::InsertWorkitem,
        ) -> Result<()> {
            M::insert_or_prioritize_workitem(self, item)
        }
        fn get_first_unclaimed_workitem(
            &self,
        ) -> Result<Option<models::Workitem>> {
            M::get_first_unclaimed_workitem(self)
        }
        fn claim_workitem(&self, uuid: Uuid) -> Result<()> {
            M::claim_workitem(self, uuid)
        }
        fn remove_workitem(&self, uuid: Uuid) -> Result<()> {
            M::remove_workitem(self, uuid)
        }
        fn get_view_month_none(&self) -> Result<models::ViewMonth> {
            M::get_view_month_none(self)
        }
        fn get_photos_without_moment(
            &self,
            paging: Paging,
        ) -> Result<Vec<models::Photo>> {
            M::get_photos_without_moment(self, paging)
        }
        fn get_view_month(
            &self,
            year: i32,
            month: i16,
        ) -> Result<models::ViewMonth> {
            M::get_view_month(self, year, month)
        }
        fn get_photos_for_timeframe(
            &self,
            start: DateTime<Utc>,
            end: DateTime<Utc>,
            paging: Paging,
        ) -> Result<Vec<models::Photo>> {
            M::get_photos_for_timeframe(self, start, end, paging)
        }
        fn get_view_year_months(
            &self,
            year: i32,
        ) -> Result<Vec<models::ViewMonth>> {
            M::get_view_year_months(self, year)
        }
    }
}
