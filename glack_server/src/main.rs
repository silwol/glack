#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate diesel;

use actix_web::{middleware::normalize, web, App, HttpServer};
use crossbeam_channel::{bounded, select, Receiver, Sender};
use diesel::pg::PgConnection;
use diesel::r2d2::{self, ConnectionManager};
use log::{info, warn};
use serde_hex::{SerHex, Strict};
use std::collections::BTreeMap;
use std::env;
use std::path::{Path, PathBuf};
use std::thread::JoinHandle;
use structopt::StructOpt;
use thiserror::Error;
use uuid::Uuid;

mod actions;
mod filestorage;
mod models;
mod schema;
mod schema_views;
mod website;

type Result<T, E = GlackError> = std::result::Result<T, E>;
type DbPool = r2d2::Pool<ConnectionManager<PgConnection>>;
type WorkItemSender = Sender<()>;
type WorkItemReceiver = Receiver<()>;

use actions::Actions;
use filestorage::FileStorage;

const PROGRAM_VERSION: &str = env!("CARGO_PKG_VERSION");
const PROGRAM_NAME: &str = "Glack";
const PROGRAM_HOMEPAGE: &str = env!("CARGO_PKG_HOMEPAGE");
const THUMBNAIL_MAXSIZE: u32 = 300;
const THUMBNAIL_QUALITY: u8 = 90;
const PAGE_SIZE: usize = 120;

#[derive(Clone, Debug, Deserialize)]
pub struct Sha1Sum(#[serde(with = "SerHex::<Strict>")] [u8; 20]);

impl AsRef<[u8; 20]> for Sha1Sum {
    fn as_ref(&self) -> &[u8; 20] {
        &self.0
    }
}

impl Sha1Sum {
    fn from_slice(s: &[u8]) -> Result<Self> {
        if s.len() != 20 {
            Err(GlackError::InvalidSha1SumLength)
        } else {
            let mut r = [0u8; 20];
            r.copy_from_slice(s);
            Ok(Self(r))
        }
    }

    fn format_hex(&self) -> String {
        let &Self(s) = self;
        format!(
            "\
    {:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}\
    {:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}",
            s[0],
            s[1],
            s[2],
            s[3],
            s[4],
            s[5],
            s[6],
            s[7],
            s[8],
            s[9],
            s[10],
            s[11],
            s[12],
            s[13],
            s[14],
            s[15],
            s[16],
            s[17],
            s[18],
            s[19],
        )
    }

    fn to_vec(&self) -> Vec<u8> {
        self.0.to_vec()
    }
}

#[derive(Error, Debug)]
pub enum GlackError {
    #[error("DotEnv error")]
    DotEnv {
        #[from]
        source: dotenv::Error,
    },
    #[error("IO error")]
    Io {
        #[from]
        source: std::io::Error,
    },
    #[error("R2D2 error")]
    R2D2 {
        #[from]
        source: ::r2d2::Error,
    },
    #[error("Var error")]
    Var {
        #[from]
        source: std::env::VarError,
    },
    #[error("Diesel error")]
    Diesel {
        #[from]
        source: diesel::result::Error,
    },
    #[error("Error persisting temporary file")]
    TemporaryFilePersistError {
        #[from]
        source: tempfile::PersistError,
    },
    #[error("Image error")]
    ImageError {
        #[from]
        source: image::ImageError,
    },
    #[error("Rexiv2 error")]
    Rexiv2Error {
        #[from]
        source: rexiv2::Rexiv2Error,
    },
    #[error("Chrono format parse error")]
    ChronoFormatParseError {
        #[from]
        source: chrono::format::ParseError,
    },
    #[error("Actix Threadpool Blocking Error: {:?}", msg)]
    ActixThreadpoolBlockingError { msg: String },
    #[error("File too large")]
    FileTooLarge,
    #[error("The object path \"{:?}\" is not a directory", filespath)]
    FilesPathNotADirectory { filespath: PathBuf },
    #[error("Not found")]
    NotFound,
    #[error("Invalid length of SHA1 sum")]
    InvalidSha1SumLength,
    #[error("Inconsisten photo assignment")]
    InconsistentPhotoAssignment,
    #[error("No EXIF data found")]
    NoExifDataFound,
    #[error("Invalid date range")]
    InvalidDateRange,
}

impl<T: std::fmt::Debug> From<actix_threadpool::BlockingError<T>>
    for GlackError
{
    fn from(e: actix_threadpool::BlockingError<T>) -> Self {
        GlackError::ActixThreadpoolBlockingError {
            msg: format!("{:?}", e),
        }
    }
}

#[derive(Debug, StructOpt)]
#[structopt(
    name = "glack_server",
    about = "An HTTP server providing the glack API"
)]
struct Opt {
    #[structopt(
        short = "o",
        long = "filespath",
        help = "The path where the files get stored",
        parse(from_os_str)
    )]
    filespath: PathBuf,
    #[structopt(
        short = "e",
        long = "envfile",
        help = "The path of a .env file containing the DATABASE_URL setting"
    )]
    envfile: String,
}

#[derive(Debug, PartialEq)]
struct ProgramInfo {
    name: &'static str,
    version: &'static str,
    homepage: &'static str,
    thumbnail_maxsize: u32,
}

impl Default for ProgramInfo {
    fn default() -> Self {
        ProgramInfo {
            name: PROGRAM_NAME,
            version: PROGRAM_VERSION,
            homepage: PROGRAM_HOMEPAGE,
            thumbnail_maxsize: THUMBNAIL_MAXSIZE,
        }
    }
}

#[derive(Debug, Deserialize)]
struct Sha1sumDeserialize {
    #[serde(with = "SerHex::<Strict>")]
    sha1sum: [u8; 20],
}

#[derive(Debug, Copy, Clone)]
pub struct Paging {
    page: usize,
    page_size: usize,
    count: usize,
}

impl Paging {
    pub fn limit(&self) -> usize {
        self.page_size
    }

    pub fn offset(&self) -> usize {
        self.page * self.page_size
    }

    pub fn pages(&self) -> usize {
        self.count / self.page_size
    }
}

fn prepare_server(
    db_pool: DbPool,
    storage: FileStorage,
    work_item_sender: WorkItemSender,
    notification_register_sender: Sender<(Uuid, Sender<Sha1Sum>)>,
) -> Result<actix_web::dev::Server> {
    Ok(HttpServer::new(move || {
        App::new()
            .wrap(normalize::NormalizePath::new(
                normalize::TrailingSlash::Trim,
            ))
            .configure(|conf| {
                website::config(
                    conf,
                    db_pool.clone(),
                    storage.clone(),
                    work_item_sender.clone(),
                    notification_register_sender.clone(),
                );
            })
            /*
                .service(web::scope("/api/v1").configure(|conf| {
                    api_v1::config(conf, db_pool.clone(), storage.clone());
                }))
            */
            .default_service(web::route().to(website::err_404))
    })
    .disable_signals()
    .bind("[::]:8080")?
    .run())
}

#[actix_rt::main]
async fn run_server(
    start_sender: Sender<Result<()>>,
    stop_receiver: Receiver<()>,
    stopped_sender: Sender<Result<()>>,
    work_item_sender: WorkItemSender,
    notification_register_sender: Sender<(Uuid, Sender<Sha1Sum>)>,
    db_pool: DbPool,
    storage: FileStorage,
) {
    match prepare_server(
        db_pool,
        storage,
        work_item_sender,
        notification_register_sender,
    ) {
        Ok(server) => {
            start_sender.send(Ok(())).ok();
            stop_receiver.recv().ok();
            server.stop(true).await;
            stopped_sender.send(Ok(())).ok();
        }
        Err(e) => {
            start_sender.send(Err(e)).ok();
        }
    }
}

fn extract_thumbnails(
    db_pool: DbPool,
    storage: FileStorage,
    stop_receiver: Receiver<()>,
    stopped_sender: Sender<()>,
    notification_register_receiver: Receiver<(Uuid, Sender<Sha1Sum>)>,
    work_item_receiver: WorkItemReceiver,
) {
    const MAX_WORKERS: usize = 50;

    let mut workers = Vec::new();

    struct Worker {
        stop_sender: Sender<()>,
        thread: JoinHandle<()>,
        name: String,
    }

    let (notification_processed_sender, notification_processed_receiver) =
        bounded(10);

    for i in 0..MAX_WORKERS {
        let (stop_sender, stop_receiver) = bounded(1);
        let name = format!("thumbnail-extractor-{}", i);

        let db_pool = db_pool.clone();
        let storage = storage.clone();
        let work_item_receiver = work_item_receiver.clone();
        let notification_processed_sender =
            notification_processed_sender.clone();
        let thread_name = name.clone();
        let thread = std::thread::spawn(move || {
            extract_thumbnails_worker(
                db_pool,
                storage,
                stop_receiver,
                work_item_receiver,
                notification_processed_sender,
                thread_name,
            );
        });
        workers.push(Worker {
            stop_sender,
            thread,
            name,
        });
    }

    let mut notification_senders =
        BTreeMap::<Uuid, Vec<Sender<Sha1Sum>>>::new();

    'outer: loop {
        select! {
            recv(stop_receiver) -> _msg => {
                // It's ok to stop uconditionally here, because if the
                // sender disconnected, then we would have to shutdown
                // anyway.
                break 'outer;
            }
            recv(notification_register_receiver) -> msg => {
                match msg {
                    Ok((uuid, sender)) => {
                        let senders = notification_senders.entry(uuid).or_insert_with(Vec::new);
                        senders.push(sender);
                    }
                    Err(e) => {
                        warn!("Notification register receiver can't read: {:?}", e);
                    }
                }
            }
            recv(notification_processed_receiver) -> msg => {
                match msg {
                    Ok((uuid, sha1sum)) => {
                        if let Some(senders) = notification_senders.remove(&uuid) {
                            for sender in senders {
                                match sender.send(sha1sum.clone()) {
                                    Ok(()) => {
                                    },
                                    Err(e) => {
                                        warn!("Can't send notification for uuid {}: {:?}", uuid, e);
                                    }
                                }
                            }
                        }
                    }
                    Err(e) => {
                        warn!("Notification processed receiver can't read: {:?}", e);
                    }
                }
            }
        }
    }

    for worker in workers.iter() {
        match worker.stop_sender.send(()) {
            Ok(()) => {}
            Err(e) => {
                warn!(
                    "Can't send stop signal to worker {:?}: {:?}",
                    worker.name, e
                );
            }
        }
    }

    for worker in workers {
        match worker.thread.join() {
            Ok(()) => {}
            Err(e) => {
                warn!(
                    "Couldn't join worker {:?} maybe it panicked?\n{:?}",
                    worker.name, e
                );
            }
        }
    }

    stopped_sender.send(()).ok();
}

fn claim_next_workitem(db_pool: &DbPool) -> Result<Option<Uuid>> {
    let p = db_pool;
    db_pool.transaction(|| {
        let workitem = p.get_first_unclaimed_workitem()?;
        if let Some(models::Workitem { photo_uuid, .. }) = workitem {
            p.claim_workitem(photo_uuid)?;
            Ok(Some(photo_uuid))
        } else {
            Ok(None)
        }
    })
}

fn extract_thumbnails_worker(
    db_pool: DbPool,
    storage: FileStorage,
    stop_receiver: Receiver<()>,
    work_item_receiver: WorkItemReceiver,
    notification_sender: Sender<(Uuid, Sha1Sum)>,
    worker_name: String,
) {
    loop {
        select! {
            recv(stop_receiver) -> _msg => {
                info!("Stopping worker: {:?}", worker_name);
                return;
            },
            recv(work_item_receiver) -> msg => {
                match msg {
                    Ok(()) => {
                        'process_queue: loop {
                            match claim_next_workitem(&db_pool) {
                                Ok(Some(uuid)) => {
                                    info!("Worker {:?} starting to work on {}", worker_name, uuid);


                                    match extract_thumbnail(&db_pool, &storage, uuid) {
                                        Ok(sha1sum) => {
                                            info!("Worker {:?} extracted thumbnail \
                                                for {}",
                                                worker_name, uuid);
                                            match notification_sender.send((uuid, sha1sum)) {
                                                Ok(()) => {},
                                                Err(e) => {
                                                    warn!("Couldn't send notification \
                                                        from worker {:?}: {:?}",
                                                        worker_name, e);
                                                }
                                            }
                                        }
                                        Err(e) => {
                                            warn!("Worker {:?} couldn't extract \
                                    thumbnail for {}: {:?}",
                                    worker_name, uuid, e);
                                        }
                                    }
                                }
                                Ok(None) => {
                                    info!("Worker {:?} has no more work to do", worker_name);
                                    break 'process_queue;
                                }
                                Err(e) => {
                                    warn!("Worker {:?} couldn't claim a work item: {:?}", worker_name, e);
                                }
                            }
                        }
                    }
                    Err(e) => {
                        warn!("{}", e);
                    }
                }
            }
        }
    }
}

fn extract_thumbnail<A: Actions>(
    a: &A,
    storage: &FileStorage,
    uuid: Uuid,
) -> Result<Sha1Sum> {
    let (file, _) = a.get_file_for_photo_thumbnail_creation(uuid)?;
    let format = image::ImageFormat::from_path(&file.filename)?;
    let mut reader = image::io::Reader::open(
        storage.file_path(&Sha1Sum::from_slice(&file.sha1sum)?),
    )?;
    reader.set_format(format);
    let image = reader.decode()?;

    let thumbnail = image.thumbnail(THUMBNAIL_MAXSIZE, THUMBNAIL_MAXSIZE);

    let mut tempfile = storage.build_temp_file()?;

    thumbnail.write_to(
        &mut tempfile,
        image::ImageOutputFormat::Jpeg(THUMBNAIL_QUALITY),
    )?;
    let filestorage::FileDetails { sha1sum, count } =
        storage.persist_temp_file(tempfile)?;

    let filename = "thumbnail.jpg";
    let mimetype = "image/jpeg";

    let db_files = vec![models::File {
        sha1sum: sha1sum.to_vec(),
        bytesize: count as i64,
        photo_uuid: uuid,
        filename: filename.to_string(),
        mimetype: mimetype.to_string(),
        role: "thumbnail".to_string(),
        moment: file.moment,
    }];

    a.transaction(|| {
        a.remove_workitem(uuid)?;
        a.insert_files(&db_files)?;
        Ok(())
    })?;

    Ok(sha1sum)
}

fn main() -> Result<()> {
    env_logger::init();

    let opt = Opt::from_args();

    let filespath = Path::new(&opt.filespath).to_path_buf();
    if !filespath.is_dir() {
        return Err(GlackError::FilesPathNotADirectory { filespath });
    }
    info!("Using storage path {:?}.", filespath);
    let storage = FileStorage::new(filespath);

    dotenv::from_path(&opt.envfile)?;
    info!("Using database env file {:?}.", opt.envfile);

    let database_url = env::var("DATABASE_URL")?;

    let db_manager = ConnectionManager::<PgConnection>::new(database_url);
    info!("Connecting to database…");
    let db_pool = r2d2::Pool::builder().max_size(50).build(db_manager)?;
    info!("Connected to database.");

    let (server_start_sender, server_start_receiver) = bounded(1);
    let (server_stop_sender, server_stop_receiver) = bounded(1);
    let (server_stopped_sender, server_stopped_receiver) = bounded(1);
    let (extractor_stop_sender, extractor_stop_receiver) = bounded(1);
    let (extractor_stopped_sender, extractor_stopped_receiver) = bounded(1);
    let (work_item_sender, work_item_receiver) = bounded(1);
    let (notification_register_sender, notification_register_receiver) =
        bounded(30);

    {
        let db_pool = db_pool.clone();
        let storage = storage.clone();
        std::thread::spawn(move || {
            run_server(
                server_start_sender,
                server_stop_receiver,
                server_stopped_sender,
                work_item_sender,
                notification_register_sender,
                db_pool,
                storage,
            );
        });
    }

    server_start_receiver.recv().unwrap().unwrap();

    {
        let db_pool = db_pool.clone();
        let storage = storage.clone();
        let notification_register_receiver =
            notification_register_receiver.clone();
        std::thread::spawn(move || {
            extract_thumbnails(
                db_pool,
                storage,
                extractor_stop_receiver,
                extractor_stopped_sender,
                notification_register_receiver,
                work_item_receiver,
            );
        });
    }

    let signals = signal_hook::iterator::Signals::new(&[
        signal_hook::SIGTERM,
        signal_hook::SIGINT,
        signal_hook::SIGQUIT,
    ])
    .unwrap();
    'outer: loop {
        for signal in signals.pending() {
            match signal as libc::c_int {
                signal_hook::SIGTERM
                | signal_hook::SIGINT
                | signal_hook::SIGQUIT => {
                    println!("Shutting down");
                    server_stop_sender.send(()).ok();
                    extractor_stop_sender.send(()).ok();
                    break 'outer;
                }
                _ => unreachable!(),
            }
        }
    }
    server_stopped_receiver.recv().ok();
    extractor_stopped_receiver.recv().ok();
    Ok(())
}
