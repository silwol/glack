// The views don't produce a table entry by itself

table! {
    view_months (year, month) {
        year -> Nullable<Int4>,
        month -> Nullable<Int2>,
        count -> Int8,
    }
}
