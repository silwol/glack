use crate::{models, GlackError, Paging, Result, Sha1Sum};
use chrono::{DateTime, Utc};
use diesel::expression_methods::ExpressionMethods;
use diesel::pg::PgConnection as Conn;
use diesel::query_dsl::{QueryDsl, RunQueryDsl};
use uuid::Uuid;

pub trait Actions {
    fn transaction<T, F: FnOnce() -> Result<T>>(&self, f: F) -> Result<T>;
    fn get_view_months(&self) -> Result<Vec<models::ViewMonth>>;
    fn get_files_for_photo(
        &self,
        photo_uuid: Uuid,
    ) -> Result<Vec<models::File>>;
    fn get_file(&self, sha1sum: &Sha1Sum) -> Result<models::File>;
    fn get_photo_thumbnail(
        &self,
        photo_uuid: Uuid,
    ) -> Result<(models::File, models::PhotoFileRole)>;
    fn get_file_for_photo_thumbnail_creation(
        &self,
        photo_uuid: Uuid,
    ) -> Result<(models::File, models::Mimetype)>;
    fn insert_files(&self, files: &[models::File]) -> Result<()>;
    fn get_mimetypes(&self) -> Result<Vec<models::Mimetype>>;
    fn insert_photos(&self, photos: &[models::Photo]) -> Result<()>;
    fn insert_mimetypes(&self, mimetypes: &[models::Mimetype]) -> Result<()>;
    fn insert_workitem(&self, item: models::InsertWorkitem) -> Result<()>;
    fn insert_or_prioritize_workitem(
        &self,
        item: models::InsertWorkitem,
    ) -> Result<()>;
    fn get_first_unclaimed_workitem(
        &self,
    ) -> Result<Option<models::Workitem>>;
    fn claim_workitem(&self, uuid: Uuid) -> Result<()>;
    fn remove_workitem(&self, uuid: Uuid) -> Result<()>;
    fn get_view_month_none(&self) -> Result<models::ViewMonth>;
    fn get_photos_without_moment(
        &self,
        paging: Paging,
    ) -> Result<Vec<models::Photo>>;
    fn get_view_month(
        &self,
        year: i32,
        month: i16,
    ) -> Result<models::ViewMonth>;
    fn get_photos_for_timeframe(
        &self,
        start: DateTime<Utc>,
        end: DateTime<Utc>,
        paging: Paging,
    ) -> Result<Vec<models::Photo>>;
    fn get_view_year_months(
        &self,
        year: i32,
    ) -> Result<Vec<models::ViewMonth>>;
}

impl Actions for Conn {
    fn transaction<T, F: FnOnce() -> Result<T>>(&self, f: F) -> Result<T> {
        use diesel::connection::Connection;
        Connection::transaction(self, f)
    }

    fn get_view_months(&self) -> Result<Vec<models::ViewMonth>> {
        use crate::schema_views::view_months::dsl;
        use diesel::expression_methods::PgSortExpressionMethods;
        Ok(dsl::view_months
            .order((dsl::year.desc().nulls_last(), dsl::month.desc()))
            .load::<models::ViewMonth>(self)?)
    }

    fn get_files_for_photo(
        &self,
        photo_uuid: Uuid,
    ) -> Result<Vec<models::File>> {
        use crate::schema::files::dsl;
        Ok(dsl::files
            .order(dsl::sha1sum)
            .filter(dsl::photo_uuid.eq(photo_uuid))
            .load::<models::File>(self)?)
    }

    fn get_file(&self, sha1sum: &Sha1Sum) -> Result<models::File> {
        use crate::schema::files::dsl;
        Ok(dsl::files
            .filter(dsl::sha1sum.eq(sha1sum.to_vec()))
            .first::<models::File>(self)?)
    }

    fn get_photo_thumbnail(
        &self,
        photo_uuid: Uuid,
    ) -> Result<(models::File, models::PhotoFileRole)> {
        use crate::schema::files::dsl as dsl_files;
        use crate::schema::photo_file_roles::dsl as dsl_roles;
        Ok(dsl_files::files
            .filter(dsl_files::photo_uuid.eq(photo_uuid))
            .inner_join(dsl_roles::photo_file_roles)
            .filter(dsl_roles::is_thumbnail.eq(true))
            .first::<(models::File, models::PhotoFileRole)>(self)?)
    }

    fn get_file_for_photo_thumbnail_creation(
        &self,
        photo_uuid: Uuid,
    ) -> Result<(models::File, models::Mimetype)> {
        use crate::schema::files::dsl as dsl_files;
        use crate::schema::mimetypes::dsl as dsl_mimetypes;

        Ok(dsl_files::files
            .order(dsl_files::sha1sum)
            .filter(dsl_files::photo_uuid.eq(photo_uuid))
            .inner_join(dsl_mimetypes::mimetypes)
            .filter(dsl_mimetypes::can_build_thumbnail.eq(true))
            .first::<(models::File, models::Mimetype)>(self)?)
    }

    fn insert_files(&self, files: &[models::File]) -> Result<()> {
        use crate::schema::files::dsl;
        diesel::insert_into(dsl::files)
            .values(files)
            .on_conflict(dsl::sha1sum)
            .do_nothing()
            .execute(self)?;
        Ok(())
    }

    fn get_mimetypes(&self) -> Result<Vec<models::Mimetype>> {
        use crate::schema::mimetypes::dsl;
        Ok(dsl::mimetypes.load::<models::Mimetype>(self)?)
    }

    fn insert_photos(&self, photos: &[models::Photo]) -> Result<()> {
        use crate::schema::photos::dsl;
        diesel::insert_into(dsl::photos)
            .values(photos)
            .execute(self)?;
        Ok(())
    }

    fn insert_mimetypes(&self, mimetypes: &[models::Mimetype]) -> Result<()> {
        use crate::schema::mimetypes::dsl;
        diesel::insert_into(dsl::mimetypes)
            .values(mimetypes)
            .on_conflict(dsl::mimetype)
            .do_nothing()
            .execute(self)?;
        Ok(())
    }

    fn insert_workitem(&self, item: models::InsertWorkitem) -> Result<()> {
        use crate::schema::workitems::dsl;
        diesel::insert_into(dsl::workitems)
            .values(item)
            .on_conflict(dsl::photo_uuid)
            .do_nothing()
            .execute(self)?;
        Ok(())
    }

    fn insert_or_prioritize_workitem(
        &self,
        item: models::InsertWorkitem,
    ) -> Result<()> {
        use crate::schema::workitems::dsl;
        diesel::insert_into(dsl::workitems)
            .values(item)
            .on_conflict(dsl::photo_uuid)
            .do_update()
            .set(dsl::priority.eq(dsl::priority + 1))
            .execute(self)?;
        Ok(())
    }

    fn get_first_unclaimed_workitem(
        &self,
    ) -> Result<Option<models::Workitem>> {
        use crate::schema::workitems::dsl;
        use diesel::result::OptionalExtension;

        Ok(dsl::workitems
            .filter(dsl::claimed.eq(false))
            .order_by(dsl::priority.desc())
            .limit(1)
            .first::<models::Workitem>(self)
            .optional()?)
    }

    fn claim_workitem(&self, uuid: Uuid) -> Result<()> {
        use crate::schema::workitems::dsl;
        diesel::update(dsl::workitems.filter(dsl::photo_uuid.eq(uuid)))
            .set(dsl::claimed.eq(true))
            .execute(self)?;
        Ok(())
    }

    fn remove_workitem(&self, uuid: Uuid) -> Result<()> {
        use crate::schema::workitems::dsl;
        diesel::delete(dsl::workitems.filter(dsl::photo_uuid.eq(uuid)))
            .execute(self)?;
        Ok(())
    }

    fn get_view_month_none(&self) -> Result<models::ViewMonth> {
        use crate::schema_views::view_months::dsl;
        use diesel::expression_methods::BoolExpressionMethods;
        Ok(dsl::view_months
            .filter(dsl::year.is_null().and(dsl::month.is_null()))
            .first::<models::ViewMonth>(self)?)
    }

    fn get_photos_without_moment(
        &self,
        paging: Paging,
    ) -> Result<Vec<models::Photo>> {
        use crate::schema::files::dsl as dsl_files;
        use crate::schema::photos::dsl as dsl_photos;

        Ok(dsl_photos::photos
            .inner_join(dsl_files::files)
            .filter(dsl_files::moment.is_null())
            .select((dsl_photos::uuid, dsl_files::moment))
            .order((dsl_files::moment, dsl_photos::uuid))
            .distinct_on((dsl_files::moment, dsl_photos::uuid))
            .select((dsl_photos::uuid,))
            .offset(paging.offset() as i64)
            .limit(paging.limit() as i64)
            .load::<models::Photo>(self)?)
    }

    fn get_view_month(
        &self,
        year: i32,
        month: i16,
    ) -> Result<models::ViewMonth> {
        use crate::schema_views::view_months::dsl;
        use diesel::expression_methods::BoolExpressionMethods;
        Ok(dsl::view_months
            .filter(dsl::year.eq(year).and(dsl::month.eq(month)))
            .first::<models::ViewMonth>(self)?)
    }

    fn get_photos_for_timeframe(
        &self,
        start: DateTime<Utc>,
        end: DateTime<Utc>,
        paging: Paging,
    ) -> Result<Vec<models::Photo>> {
        if end < start {
            Err(GlackError::InvalidDateRange)
        } else {
            use crate::schema::files::dsl as dsl_files;
            use crate::schema::photos::dsl as dsl_photos;

            Ok(dsl_photos::photos
                .inner_join(dsl_files::files)
                .filter(dsl_files::moment.between(start, end))
                .select((dsl_photos::uuid, dsl_files::moment))
                .order((dsl_files::moment, dsl_photos::uuid))
                .distinct_on((dsl_files::moment, dsl_photos::uuid))
                .select((dsl_photos::uuid,))
                .offset(paging.offset() as i64)
                .limit(paging.limit() as i64)
                .load::<models::Photo>(self)?)
        }
    }

    fn get_view_year_months(
        &self,
        year: i32,
    ) -> Result<Vec<models::ViewMonth>> {
        use crate::schema_views::view_months::dsl;
        Ok(dsl::view_months
            .filter(dsl::year.eq(year))
            .order(dsl::month.asc())
            .load::<models::ViewMonth>(self)?)
    }
}
