extern crate regex;
#[macro_use]
extern crate serde_derive;

use regex::Regex;

#[derive(Serialize, Deserialize, Debug)]
pub struct Object {
    pub sha1sum: [u8; 20],
    pub mimetype: String,
}

pub fn is_sha1sum(s: &str) -> bool {
    let re = Regex::new(r"^[0-9a-fA-F]{40}$").unwrap();
    re.is_match(s)
}

pub fn parse_sha1sum(s: &str) -> Option<[u8; 20]> {
    if is_sha1sum(s) {
        let mut a = [0u8; 20];
        let v: Vec<u8> = s.as_bytes()
            .chunks(2)
            .map(|s| String::from_utf8(s.to_vec()).unwrap())
            .map(|s| u8::from_str_radix(&s, 16).unwrap())
            .collect();
        a.copy_from_slice(&v[0..20]);
        Some(a)
    } else {
        None
    }
}
